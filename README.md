My name is Kira Singla, and my pronouns are They/Them. 
I'm an undergrad at the University of Maryland, Baltimore County studying computer engineering on the communications track. 
My main focus is applied and computational mathematics and I primarily use the programming languages Fortran and Julia. 
Outside of this field, my interests include poetry, photography, and deconstructionist philosophy. 

My repositories on this account include school projects such as `cmpe320-project-1` and `cmpe323-lab-1`, and personal projects such as `simple-numerical-integration`.

On a research level, I am interested in dynamical systems, their application to controlled systems, and the implementation of these systems in solving real world problems.
I believe that the world will be a better place if we do not need to depend on large hierarchical systems that can be easily controlled and defeated by hostile actors.
To this extent I advocate for repairable and efficient personal computers, democratized access to the internet, and open source hardware and software.
